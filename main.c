#pragma ide diagnostic ignored "ArgumentSelectionDefects"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include "matrices/arrays.h"
#include "matrices/matrices.h"

//https://www.geeksforgeeks.org/how-to-measure-time-taken-by-a-program-in-c/
#define MEASURE_TIME(func, label)     \
    start = clock();        \
    {func;}                     \
    end = clock();          \
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC; \
    printf(label " time: %f sec\n", cpu_time_used);

void *readFirstFile(void *fileName);

void *readSecondFile(void *fileName);

char *firstFile, *secondFile, *outFile;

void initializeArguments(int argc, char **argv);

int main(int argc, char *argv[]) {

    initializeArguments(argc, argv);

    clock_t start, end;
    double cpu_time_used;

    puts("");
    MEASURE_TIME(
            pthread_t parallelReading;
            pthread_create(&parallelReading, NULL, readFirstFile, firstFile);
            readSecondFile(secondFile);

            pthread_join(parallelReading, NULL), "Reading")

    puts("");
    if (matrixA.m != matrixB.m) {
        perror("Cannot perform multiplication!");
        exit(0);
    }

    matrixC.n = matrixA.n;
    matrixC.m = matrixB.n;
    ALLOCATE_MATRIX(matrixC.arr, matrixC.n, matrixC.m)

    MEASURE_TIME(
            multiply_no_threads(0), "No Threads")
    MEASURE_TIME(
            multiply_row_threads(0), "Row Threads")
    MEASURE_TIME(
            multiply_element_threads(0), "Elements Threads")

    puts("");
    puts("");
    MEASURE_TIME(
            multiply_no_threads(0);
            normalFprintMatrix(matrixC, outFile), "No Threads + Print (After Calculations)")
    MEASURE_TIME(
            multiply_no_threads(outFile), "No Threads + Print (After Calculations: StringBuilder)")
    puts("");
    MEASURE_TIME(
            multiply_row_threads(0);
            normalFprintMatrix(matrixC, outFile), "Row Threads + Print (After Calculations)")
    MEASURE_TIME(
            multiply_row_threads(outFile), "Row Threads + Print (After Each Row: StringBuilder)")
    puts("");
    MEASURE_TIME(
            multiply_element_threads(0);
            normalFprintMatrix(matrixC, outFile), "Elements Threads + Print (After Calculations)")
    MEASURE_TIME(
            multiply_element_threads(outFile), "Elements Threads + Print (After Each Element)")
    puts("");

    FREE_MATRIX(matrixA.arr, matrixA.n);
    FREE_MATRIX(matrixB.arr, matrixB.n);
    FREE_MATRIX(matrixC.arr, matrixC.n);

    return 0;
}

void initializeArguments(int argc, char **argv) {
    if (argc > 3) {
        firstFile = argv[1];
        secondFile = argv[2];
        outFile = argv[3];
    } else {
        firstFile = "a.txt";
        secondFile = "b.txt";
        outFile = "c.out";
    }
}

void *readFirstFile(void *fileName) {
    fileName = (char *) fileName;

    // Map file into memory
    // https://youtu.be/m7E9piHcfr4
    int fileDescriptorA = open(fileName, O_RDONLY);
    struct stat fileStatesA;

    if (fstat(fileDescriptorA, &fileStatesA) == -1)
        perror("Can't get file attributes");

    char *fileMapA = mmap(NULL, fileStatesA.st_size, PROT_READ, MAP_PRIVATE, fileDescriptorA, 0);

    // Get matrix dimensions
    getArrayDim(fileMapA, fileStatesA.st_size, &(matrixA.n), &(matrixA.m));

    // Populate matrix
    ALLOCATE_MATRIX(matrixA.arr, matrixA.n, matrixA.m)
    stringTo2DIntArray(matrixA.arr, fileMapA, fileStatesA.st_size);

    close(fileDescriptorA);
    munmap(fileMapA, fileStatesA.st_size);
    return NULL;
}

void *readSecondFile(void *fileName) {
    fileName = (char *) fileName;

    // Map file into memory
    // https://youtu.be/m7E9piHcfr4
    int fileDescriptorB = open(fileName, O_RDONLY);
    struct stat fileStatesB;

    if (fstat(fileDescriptorB, &fileStatesB) == -1)
        perror("Can't get file attributes");

    char *fileMapB = mmap(NULL, fileStatesB.st_size, PROT_READ, MAP_PRIVATE, fileDescriptorB, 0);

    // Get matrix dimensions: suppress warning about swapping m and n,
    // they were swapped to give more flexibility in the calculations.
    getArrayDim(fileMapB, fileStatesB.st_size, &(matrixB.m), &(matrixB.n));

    // Populate matrix
    ALLOCATE_MATRIX(matrixB.arr, matrixB.n, matrixB.m)
    stringTo2DIntArrayTranspose(matrixB.arr, fileMapB, fileStatesB.st_size);

    close(fileDescriptorB);
    munmap(fileMapB, fileStatesB.st_size);
    return NULL;
}
