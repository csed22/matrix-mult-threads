//
// Created by Ahmed Waleed on 11/18/20.
//
#include <sys/stat.h>
#include <stdbool.h>
#include "arrays.h"

void getArrayDim(const char *string, __off_t stringSize, int *n, int *m) {

    *n = 0;
    *m = 0;

    for (int k = 0; k < stringSize; k++) {
        char c = string[k];
        switch (c) {
            case '\n':
                *n = *n + 1;
                *m = *m + 1;
                break;
            case ' ':
            case '\t':
                *m = *m + 1;
                break;
            default:
                break;
        }
    }

    *m = *m / *n;

}

void stringTo2DIntArray(int **arr, const char *string, __off_t stringSize) {

    int i = 0;
    int j = 0;
    int num = 0;
    bool isNegative = false;

    for (int k = 0; k < stringSize; k++) {
        char c = string[k];
        switch (c) {
            case '\n':
                arr[i++][j] = num * (isNegative ? -1 : 1);
                j = 0;
                num = 0;
                isNegative = false;
                break;
            case ' ':
            case '\t':
                arr[i][j++] = num;
                num = 0;
                isNegative = false;
                break;
            case '-':
                isNegative = true;
            default:
                num = num * 10 + c - '0';
        }
    }

}

void stringTo2DIntArrayTranspose(int **arr, const char *string, __off_t stringSize) {

    int i = 0;
    int j = 0;
    int num = 0;
    bool isNegative = false;

    for (int k = 0; k < stringSize; k++) {
        char c = string[k];
        switch (c) {
            case '\n':
                arr[i][j++] = num * (isNegative ? -1 : 1);
                i = 0;
                num = 0;
                isNegative = false;
                break;
            case ' ':
            case '\t':
                arr[i++][j] = num;
                num = 0;
                isNegative = false;
                break;
            case '-':
                isNegative = true;
            default:
                num = num * 10 + c - '0';
        }
    }

}
