//
// Created by Ahmed Waleed on 11/18/20.
//

#ifndef MATRIX_MULTIPLICATION_ARRAYS_H
#define MATRIX_MULTIPLICATION_ARRAYS_H

#include <stdint.h>

void getArrayDim(const char *string, __off_t stringSize, int *n, int *m);

void stringTo2DIntArray(int **arr, const char *string, __off_t stringSize);

void stringTo2DIntArrayTranspose(int **arr, const char *string, __off_t stringSize);

#endif //MATRIX_MULTIPLICATION_ARRAYS_H
