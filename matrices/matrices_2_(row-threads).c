//
// Created by Ahmed Waleed on 11/19/20.
//

#include <malloc.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include "matrices.h"
#include "../c-utilities/StringBuilder.h"

bool *thread_flag_row;

void *fprintRows(void *outFile) {
    outFile = (char *) outFile;
    FILE *file = fopen(outFile, "w");

    for (int i = 0; i < matrixC.n; i++) {
        while (!thread_flag_row[i]) {}
        struct StringBuilder *sb = new_StringBuilder();

        for (int j = 0; j < matrixC.m; j++) {
            int num = matrixC.arr[i][j];
            if (j < matrixC.m - 1)
                // consider \t and \0
                StringBuilder.append(sb, "%d\t", num);
            else if (i < matrixC.n - 1)
                // last number in row does not have \t after it
                StringBuilder.append(sb, "%d\n", num);
            else
                // last number in the last row does not have \n.
                StringBuilder.append(sb, "%d", num);
        }

        fprintf(file, "%s", StringBuilder.toString(sb));
        free_StringBuilder(sb);
    }

    fclose(file);
    return NULL;
}

struct row_data {
    int i;
};

void *calculateRow(void *row_data) {
    struct row_data *data = row_data;

    for (int j = 0; j < matrixC.m; j++) {
        matrixC.arr[data->i][j] = 0;
        for (int k = 0; k < commonDim; k++) {
            matrixC.arr[data->i][j] += matrixA.arr[data->i][k] * matrixB.arr[j][k];
        }
    }

    thread_flag_row[data->i] = true;
    free(data);

    return NULL;
}

void multiply_row_threads(char *outFile) {

    commonDim = matrixA.m;
    thread_flag_row = malloc(matrixC.n * sizeof(bool));

    for (int i = 0; i < matrixC.n; i++)
        thread_flag_row[i] = false;

    pthread_t printWhenReady;
    if (outFile)
        pthread_create(&printWhenReady, NULL, fprintRows, outFile);

    for (int i = 0; i < matrixC.n; i++) {
        struct row_data *data = malloc(sizeof(struct row_data));
        data->i = i;

        pthread_t parallelCalculate;
        pthread_create(&parallelCalculate, NULL, calculateRow, data);
    }

    if (outFile)
        pthread_join(printWhenReady, NULL);
}
