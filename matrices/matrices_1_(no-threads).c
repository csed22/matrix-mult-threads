//
// Created by Ahmed Waleed on 11/19/20.
//

#include <malloc.h>
#include "matrices.h"
#include "../c-utilities/StringBuilder.h"

void fprintMatrix(struct matrix matrix, const char *outFile);

void multiply_no_threads(const char *outFile) {
    commonDim = matrixA.m;
    for (int i = 0; i < matrixC.n; i++) {

        for (int j = 0; j < matrixC.m; j++) {
            matrixC.arr[i][j] = 0;
            for (int k = 0; k < commonDim; k++) {
                matrixC.arr[i][j] += matrixA.arr[i][k] * matrixB.arr[j][k];
            }
        }
    }
    if (outFile)
        fprintMatrix(matrixC, outFile);
}

void normalFprintMatrix(struct matrix matrix, const char *outFile) {

    FILE *file = fopen(outFile, "w");

    for (int i = 0; i < matrix.n; i++)
        for (int j = 0; j < matrix.m; j++) {
            if (j < matrix.m - 1)
                fprintf(file, "%d\t", matrix.arr[i][j]);
            else if (i < matrix.n - 1)
                // last number in row does not have \t after it
                fprintf(file, "%d\n", matrix.arr[i][j]);
            else
                // last number in the last row does not have \n.
                fprintf(file, "%d", matrix.arr[i][j]);
        }

    fclose(file);

}

void fprintMatrix(struct matrix matrix, const char *outFile) {

    struct StringBuilder *sb = new_StringBuilder();

    for (int i = 0; i < matrix.n; i++)
        for (int j = 0; j < matrix.m; j++) {
            int num = matrix.arr[i][j];
            if (j < matrix.m - 1)
                // consider \t and \0
                StringBuilder.append(sb, "%d\t", num);
            else if (i < matrix.n - 1)
                // last number in row does not have \t after it
                StringBuilder.append(sb, "%d\n", num);
            else
                // last number in the last row does not have \n.
                StringBuilder.append(sb, "%d", num);
        }

    FILE *file = fopen(outFile, "w");
    fprintf(file, "%s", StringBuilder.toString(sb));
    fclose(file);

    free_StringBuilder(sb);
}

