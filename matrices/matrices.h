//
// Created by Ahmed Waleed on 11/18/20.
//

#include <stdio.h>

#ifndef MATRIX_MULTIPLICATION_MATRICES_H
#define MATRIX_MULTIPLICATION_MATRICES_H

// https://stackoverflow.com/questions/3911400/how-to-pass-2d-array-matrix-in-a-function-in-c
#define ALLOCATE_MATRIX(x, n, m) \
    x = malloc(n * sizeof *x);\
    for (int i=0; i < n; i++)\
        x[i] = malloc(m * sizeof *x[i]);

//https://stackoverflow.com/questions/5666214/how-to-free-2d-array-in-c
#define FREE_MATRIX(x, n) \
    for (int i = 0; i < n; i++)\
        free(x[i]);\
    free(x)

struct matrix {
    int **arr;
    int n;
    int m;
} matrixA, matrixB, matrixC;
int commonDim;

void multiply_no_threads(const char *outFile);

void multiply_row_threads(char *outFile);

void multiply_element_threads(char *outFile);

void normalFprintMatrix(struct matrix matrix, const char *outFile);

#endif //MATRIX_MULTIPLICATION_MATRICES_H
