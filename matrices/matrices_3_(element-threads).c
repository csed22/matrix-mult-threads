//
// Created by Ahmed Waleed on 11/19/20.
//

#include <malloc.h>
#include <pthread.h>
#include <stdbool.h>
#include "matrices.h"

bool **thread_flag_elem;

void *fprintElement(void *outFile) {
    outFile = (char *) outFile;
    FILE *file = fopen(outFile, "w");

    for (int i = 0; i < matrixC.n; i++)
        for (int j = 0; j < matrixC.m; j++) {
            while (!thread_flag_elem[i][j]) {}
            if (j < matrixC.m - 1)
                fprintf(file, "%d\t", matrixC.arr[i][j]);
            else if (i < matrixC.n - 1)
                // last number in row does not have \t after it
                fprintf(file, "%d\n", matrixC.arr[i][j]);
            else
                // last number in the last row does not have \n.
                fprintf(file, "%d", matrixC.arr[i][j]);
        }

    fclose(file);
    return NULL;
}

struct element_data {
    int i;
    int j;
};

void *calculateElement(void *element_data) {
    struct element_data *data = element_data;

    matrixC.arr[data->i][data->j] = 0;
    for (int k = 0; k < commonDim; k++) {
        matrixC.arr[data->i][data->j] += matrixA.arr[data->i][k] * matrixB.arr[data->j][k];
    }

    thread_flag_elem[data->i][data->j] = true;
    free(data);

    return NULL;
}

void multiply_element_threads(char *outFile) {

    commonDim = matrixA.m;
    thread_flag_elem = malloc(matrixC.n * sizeof(bool *));

    for (int i = 0; i < matrixC.n; i++) {
        thread_flag_elem[i] = malloc(matrixC.m * sizeof(bool));
        for (int j = 0; j < matrixC.m; j++)
            thread_flag_elem[i][j] = false;
    }

    pthread_t printWhenReady;
    if (outFile)
        pthread_create(&printWhenReady, NULL, fprintElement, outFile);

    for (int i = 0; i < matrixC.n; i++) {
        for (int j = 0; j < matrixC.m; j++) {
            struct element_data *data = malloc(sizeof(struct element_data));
            data->i = i;
            data->j = j;

            pthread_t parallelCalculate;
            pthread_create(&parallelCalculate, NULL, calculateElement, data);
        }
    }

    if (outFile)
        pthread_join(printWhenReady, NULL);
}
